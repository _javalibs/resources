package org.tastefuljava.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public enum DefaultResourceManager {
    INSTANCE;

    public ResourceManager getDefault() {
        return buildDefault(
                "org.tastefuljava.resources.path",
                "cataline.base/conf",
                "catalina.base");
    }

    private static ResourceManager buildDefault(String... path) {
        List<ResourceManager> list = new ArrayList<>();
        for (String el: path) {
            addToList(el, list);
        }
        ResourceManager cp = new ClasspathResourceManager();
        if (list.isEmpty()) {
            return cp;
        }
        list.add(cp);
        return new CascadingResourceManager(
                list.toArray(new ResourceManager[0]));
    }

    private static void addToList(String el, List<ResourceManager> list) {
        int ix = el.indexOf('/');
        String prop = ix < 0 ? el : el.substring(0, ix);
        String path = System.getProperty(prop);
        if (path != null) {
            for (String dirName: path.split(";")) {
                File dir = new File(dirName);
                if (ix >= 0) {
                    dir = new File(dir, el.substring(ix+1));
                }
                if (dir.isDirectory()) {
                    list.add(new FileResourceManager(dir));
                }
            }
        }
    }
}
