package org.tastefuljava.resources;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class FileResourceManager implements ResourceManager {
    private final File dir;

    public FileResourceManager(File dir) {
        this.dir = dir;
    }

    @Override
    public URL lookup(String name) throws IOException {
        File file = new File(dir, name);
        if (!file.isFile()) {
            return null;
        }
        return file.toURI().toURL();
    }
}
