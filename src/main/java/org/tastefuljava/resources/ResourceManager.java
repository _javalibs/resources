package org.tastefuljava.resources;

import java.io.IOException;
import java.net.URL;

public interface ResourceManager {
    public URL lookup(String name) throws IOException;

    public static ResourceManager getDefault() {
        return DefaultResourceManager.INSTANCE.getDefault();
    }
}
