package org.tastefuljava.resources;

import java.io.IOException;
import java.net.URL;

public class CascadingResourceManager implements ResourceManager {
    private final ResourceManager[] list;

    public CascadingResourceManager(ResourceManager... list) {
        this.list = list;
    }

    @Override
    public URL lookup(String name) throws IOException {
        for (ResourceManager rm: list) {
            URL url = rm.lookup(name);
            if (url != null) {
                return url;
            }
        }
        return null;
    }
}
