package org.tastefuljava.resources;

import java.io.IOException;
import java.net.URL;

public class ClasspathResourceManager implements ResourceManager {
    private final ClassLoader cl;

    public ClasspathResourceManager() {
        this(Thread.currentThread().getContextClassLoader());
    }

    public ClasspathResourceManager(ClassLoader cl) {
        this.cl = cl;
    }

    @Override
    public URL lookup(String name) throws IOException {
        return cl.getResource(name);
    }
}
